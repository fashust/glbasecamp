//
//  c3t5.cpp
//  GLBaseCamp
//
//  Created by Andrew Pogrebnjak on 05.04.14.
//  Copyright (c) 2014 Andrew Pogrebnjak. All rights reserved.
//

#include "c3t5.h"

void c3t5() {
    string total_population_str, country_population_str;
    long long total_population, country_population = 0;
    
    while (true) {
        cout << "Enter the world's population: ";
        cin >> total_population_str;
        if (total_population_str == "q") break;
        total_population = convert(total_population_str, LLONG_MAX);
        if (total_population < 0) continue;
        while (true) {
            cout << "Enter the population of the country: ";
            cin >> country_population_str;
            if (country_population_str == "q") return;
            country_population = convert(country_population_str, LLONG_MAX);
            if (country_population < 0) continue;
            cout << "The population of the country is " << ((double)country_population / (double)total_population * 100.0) << " of the world population.\n";
            break;
        }
    }
}