//
//  c3t5.h
//  GLBaseCamp
//
//  Created by Andrew Pogrebnjak on 05.04.14.
//  Copyright (c) 2014 Andrew Pogrebnjak. All rights reserved.
//

#ifndef __GLBaseCamp__c3t5__
#define __GLBaseCamp__c3t5__

#include <iostream>
#include "convert.h"

using namespace std;

void c3t5();

#endif /* defined(__GLBaseCamp__c3t5__) */
