//
//  convert.cpp
//  GLBaseCamp
//
//  Created by Andrew Pogrebnjak on 05.04.14.
//  Copyright (c) 2014 Andrew Pogrebnjak. All rights reserved.
//

#include "convert.h"

long long convert(string str_val, long long max) {
    long long res;
    
    try {
        res = stoll(str_val);
    } catch (invalid_argument) {
        cout << "Invalid value \"" << str_val << "\", allowed \"q\" or number\n";
        return -1;
    }
    if (res < 0 || res > max) {
        cout << "Invalid value \"" << str_val << "\", min = 0, max = " << max << "\n";
        return -1;
    }
    return res;
}