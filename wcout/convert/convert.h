//
//  convert.h
//  GLBaseCamp
//
//  Created by Andrew Pogrebnjak on 05.04.14.
//  Copyright (c) 2014 Andrew Pogrebnjak. All rights reserved.
//

#ifndef __GLBaseCamp__convert__
#define __GLBaseCamp__convert__

#include <iostream>

using namespace std;

long long convert(string, long long);

#endif /* defined(__GLBaseCamp__convert__) */
