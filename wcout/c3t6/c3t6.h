//
//  c3t6.h
//  GLBaseCamp
//
//  Created by Andrew Pogrebnjak on 05.04.14.
//  Copyright (c) 2014 Andrew Pogrebnjak. All rights reserved.
//

#ifndef __GLBaseCamp__c3t6__
#define __GLBaseCamp__c3t6__

#include <iostream>
#include "convert.h"

using namespace std;

void c3t6();

#endif /* defined(__GLBaseCamp__c3t6__) */
