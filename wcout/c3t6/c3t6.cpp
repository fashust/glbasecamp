//
//  c3t6.cpp
//  GLBaseCamp
//
//  Created by Andrew Pogrebnjak on 05.04.14.
//  Copyright (c) 2014 Andrew Pogrebnjak. All rights reserved.
//

#include "c3t6.h"

void c3t6() {
    string kilometers_str, liters_str;
    int kilometers, liters = 0;
    
    while (true) {
        cout << "Enter a number of kilometers: ";
        cin >> kilometers_str;
        if (kilometers_str == "q") return;
        kilometers = (int)convert(kilometers_str, (long long)INT_MAX);
        if (!kilometers) continue;
        while (true) {
            cout << "Enter a number of liters: ";
            cin >> liters_str;
            if (liters_str == "q") return;
            liters = (int)convert(liters_str, (long long)INT16_MAX);
            if (!kilometers) continue;
            cout << "Litters pre 100 km = " << ((double)liters / (double)kilometers) * 100 << endl;
            break;
        }
    }
}