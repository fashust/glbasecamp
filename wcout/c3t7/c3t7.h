//
//  c3t7.h
//  GLBaseCamp
//
//  Created by Andrew Pogrebnjak on 05.04.14.
//  Copyright (c) 2014 Andrew Pogrebnjak. All rights reserved.
//

#ifndef __GLBaseCamp__c3t7__
#define __GLBaseCamp__c3t7__

#include <iostream>
#include "convert.h"

using namespace std;

const int H = 100;
const double LPG = 3.785411784;
const double KMPM = 1.609344;

void c3t7();

#endif /* defined(__GLBaseCamp__c3t7__) */
