//
//  c3t7.cpp
//  GLBaseCamp
//
//  Created by Andrew Pogrebnjak on 05.04.14.
//  Copyright (c) 2014 Andrew Pogrebnjak. All rights reserved.
//

#include "c3t7.h"

void c3t7() {
    int lphkm = 0;
    string lphkm_str;
    
    while (true) {
        cout << "Enter gasoline consumption figure in the European style: ";
        cin >> lphkm_str;
        if (lphkm_str == "q") return;
        lphkm = (int)convert(lphkm_str, (long long)INT16_MAX);
        if (lphkm < 0) continue;
        cout << "the U.S. style: " << (double)((H * LPG) / (KMPM * lphkm)) << " mpg\n";
    }
}