//
//  main.cpp
//  wcout
//
//  Created by Andrew Pogrebnjak on 04.04.14.
//  Copyright (c) 2014 Andrew Pogrebnjak. All rights reserved.
//

#include "c3t1/c3t1.h"
#include "c3t2/c3t2.h"
#include "c3t3/c3t3.h"
#include "c3t4/c3t4.h"
#include "c3t5/c3t5.h"
#include "c3t6/c3t6.h"
#include "c3t7/c3t7.h"

int main(int argc, const char * argv[]) {
    
    c3t1();
    c3t2();
    c3t3();
    c3t4();
    c3t5();
    c3t6();
    c3t7();
    return 0;
}