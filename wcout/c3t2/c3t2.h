//
//  c3t2.h
//  GLBaseCamp
//
//  Created by Andrew Pogrebnjak on 04.04.14.
//  Copyright (c) 2014 Andrew Pogrebnjak. All rights reserved.
//

#ifndef __GLBaseCamp__c3t2__
#define __GLBaseCamp__c3t2__

#include <iostream>
#include <climits>
#include <math.h>
#include "convert.h"

using namespace std;

const double INCH_IN_FT = 12;
const double M_IN_INCH = 0.0254;
const double KG_IN_PN = 2.2;

void c3t2();
double bmi(int, int, int);

#endif /* defined(__GLBaseCamp__c3t2__) */
