//
//  c3t2.cpp
//  GLBaseCamp
//
//  Created by Andrew Pogrebnjak on 04.04.14.
//  Copyright (c) 2014 Andrew Pogrebnjak. All rights reserved.
//

#include "c3t2.h"

void c3t2() {
    int ft, inch = 0;
    int pound = 0;
    string ft_str, inch_str, pound_str;
    
    while (true) {
        cout << "Enter your height ft: ";
        cin >> ft_str;
        if (ft_str == "q") break;
        ft = (int)convert(ft_str, INT_MAX);
        if (ft < 0) continue;
        while (true) {
            cout << "Enter your height inch: ";
            cin >> inch_str;
            if (inch_str == "q") break;
            inch = (int)convert(inch_str, INT_MAX);
            if (inch < 0) continue;
            while (true) {
                cout << "Enter your weight in pounds: ";
                cin >> pound_str;
                if (pound_str == "q") break;
                pound = (int)convert(pound_str, INT_MAX);
                if (pound < 0) continue;
                cout << "Your BMI: " << bmi(ft, inch, pound) << endl;
            }
        }
    }
}

double bmi(int ft, int inch, int pound) {
    double meters = (ft * INCH_IN_FT + inch) * M_IN_INCH;
    double kilos = pound / KG_IN_PN;
    
    return kilos / sqrt(meters);
}
