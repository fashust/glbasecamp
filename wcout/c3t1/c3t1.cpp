//
//  c3t1.cpp
//  GLBaseCamp
//
//  Created by Andrew Pogrebnjak on 04.04.14.
//  Copyright (c) 2014 Andrew Pogrebnjak. All rights reserved.
//

#include "c3t1.h"

void c3t1() {
    
    int inches = 0;
    string inches_str;
    
    while (true) {
        cout << "Enter your height in INCHES: ______\b\b\b\b\b\b";
        cin >> inches_str;
        if (inches_str == "q") {
            break;
        } else {
            try {
                inches = stoi(inches_str);
                if (inches < 0) {
                    throw invalid_argument("invalid argument");
                }
            } catch (invalid_argument) {
                cout << "Invalid value: \"" << inches_str << "\", allowed \"q\" or number less than " << INT_MAX << endl;
                continue;
            }
            cout << "Your height: " << inches / FT << " ft. " << inches % FT << "inch\n";
        }
    }
}