//
//  c3t1.h
//  GLBaseCamp
//
//  Created by Andrew Pogrebnjak on 04.04.14.
//  Copyright (c) 2014 Andrew Pogrebnjak. All rights reserved.
//

#ifndef __GLBaseCamp__c3t1__
#define __GLBaseCamp__c3t1__

#include <iostream>
#include <climits>

using namespace std;

const int FT = 12;

void c3t1();

#endif /* defined(__GLBaseCamp__c3t1__) */
