//
//  c3t4.cpp
//  GLBaseCamp
//
//  Created by Andrew Pogrebnjak on 05.04.14.
//  Copyright (c) 2014 Andrew Pogrebnjak. All rights reserved.
//

#include "c3t4.h"

void c3t4() {
    string seconds_str;
    long long seconds, tmp, days, hours, minutes = 0;
    
    while (true) {
        cout << "Enter the number of seconds: ";
        cin >> seconds_str;
        if (seconds_str == "q") break;
        seconds = convert(seconds_str, LLONG_MAX);
        if (seconds < 0) continue;
        days = seconds / S_IN_D;
        tmp = seconds - (days * S_IN_D);
        hours = tmp / S_IN_H;
        tmp = tmp - (hours * S_IN_H);
        minutes = tmp / S_IN_M;
        seconds = tmp - (minutes * S_IN_M);
        cout << seconds_str << " seconds = " << days << " days, " << hours << " hours, " << minutes << " minutes, " << seconds << " seconds\n";
    }
}