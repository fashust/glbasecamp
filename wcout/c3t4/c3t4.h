//
//  c3t4.h
//  GLBaseCamp
//
//  Created by Andrew Pogrebnjak on 05.04.14.
//  Copyright (c) 2014 Andrew Pogrebnjak. All rights reserved.
//

#ifndef __GLBaseCamp__c3t4__
#define __GLBaseCamp__c3t4__

#include <iostream>
#include <climits>
#include "convert.h"

using namespace std;
const long S_IN_D = 24 * 60 * 60;
const long S_IN_H = 60 * 60;
const long S_IN_M = 60;

void c3t4();

#endif /* defined(__GLBaseCamp__c3t4__) */
