//
//  c3t3.cpp
//  GLBaseCamp
//
//  Created by Andrew Pogrebnjak on 05.04.14.
//  Copyright (c) 2014 Andrew Pogrebnjak. All rights reserved.
//

#include "c3t3.h"

void c3t3() {
    string degree_str, minutes_str, seconds_str;
    int degree, minutes, seconds = 0;
    
    cout << "Enter a latitude in degrees, minutes, and seconds:\n";
    while (true) {
        cout << "First, enter the degrees: ";
        cin >> degree_str;
        if (degree_str == "q") break;
        degree = (int)convert(degree_str, MAX_DEGREE);
        if (degree < 0) continue;
        while (true) {
            cout << "Next, enter the minutes of arc: ";
            cin >> minutes_str;
            if (minutes_str == "q") break;
            minutes = (int)convert(minutes_str, MAX_MINUTES);
            if (minutes < 0) continue;
            while (true) {
                cout << "Finally, enter the seconds of arc: ";
                cin >> seconds_str;
                if (seconds_str == "q") break;
                seconds = (int)convert(seconds_str, MAX_SECONDS);
                if (seconds < 0) continue;
                cout << degree << " degrees, " << minutes << " minutes, " << seconds << " seconds = " << degree + to_degree(minutes, seconds) << " degrees\n";
                break;
            }
            break;
        }
    }
}

double to_degree(int minutes, int seconds) {
    return ((double)minutes / MINUTES_TO_DEGREE + (double)seconds / SECONDS_TO_DEEGRE);
}