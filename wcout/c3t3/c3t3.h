//
//  c3t3.h
//  GLBaseCamp
//
//  Created by Andrew Pogrebnjak on 05.04.14.
//  Copyright (c) 2014 Andrew Pogrebnjak. All rights reserved.
//

#ifndef __GLBaseCamp__c3t3__
#define __GLBaseCamp__c3t3__

#include <iostream>
#include "convert.h"

using namespace std;

const int MAX_DEGREE = 360;
const int MAX_MINUTES = 59;
const int MAX_SECONDS = 59;
const int MINUTES_TO_DEGREE = 60;
const int SECONDS_TO_DEEGRE = 3600;

void c3t3();
double to_degree(int, int);

#endif /* defined(__GLBaseCamp__c3t3__) */
