//
//  c4t3.h
//  GLBaseCamp
//
//  Created by Andrew Pogrebnjak on 05.04.14.
//  Copyright (c) 2014 Andrew Pogrebnjak. All rights reserved.
//

#ifndef __GLBaseCamp__c4t3__
#define __GLBaseCamp__c4t3__

#include <iostream>
#include <cstring>

using namespace std;

const int STRLEN = 40;

void c4t3();

#endif /* defined(__GLBaseCamp__c4t3__) */
