//
//  c4t3.cpp
//  GLBaseCamp
//
//  Created by Andrew Pogrebnjak on 05.04.14.
//  Copyright (c) 2014 Andrew Pogrebnjak. All rights reserved.
//

#include "c4t3.h"

void c4t3() {
    char first_name[STRLEN + 1], last_name[STRLEN + 1], result[STRLEN * 2 + 1];

    cout << "Enter your first name: ";
    cin.getline(first_name, STRLEN);
    cout << "Enter your last name: ";
    cin.getline(last_name, STRLEN);
    strcpy(result, last_name);
    strcat(result, ", ");
    strcat(result, first_name);
    strcat(result, "\n");
    cout << "Here’s the information in a single string: " << result;
}