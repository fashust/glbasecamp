//
//  c4t5.h
//  GLBaseCamp
//
//  Created by Andrew Pogrebnjak on 05.04.14.
//  Copyright (c) 2014 Andrew Pogrebnjak. All rights reserved.
//

#ifndef __GLBaseCamp__c4t5__
#define __GLBaseCamp__c4t5__

#include <iostream>
#include "structs.h"

using namespace std;

void c4t5();

#endif /* defined(__GLBaseCamp__c4t5__) */
