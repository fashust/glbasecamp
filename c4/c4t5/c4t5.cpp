//
//  c4t5.cpp
//  GLBaseCamp
//
//  Created by Andrew Pogrebnjak on 05.04.14.
//  Copyright (c) 2014 Andrew Pogrebnjak. All rights reserved.
//

#include "c4t5.h"

void c4t5() {
    CandyBar snack = {(char*)"Mocha Much", 2.3, 350};
    
    cout << "Brand: " << snack.brand << "\nWeight: " << snack.weight << "\nCalories: " << snack.calories << "\n";
    return;
}