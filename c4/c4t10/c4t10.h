//
//  c4t10.h
//  GLBaseCamp
//
//  Created by Andrew Pogrebnjak on 06.04.14.
//  Copyright (c) 2014 Andrew Pogrebnjak. All rights reserved.
//

#ifndef __GLBaseCamp__c4t10__
#define __GLBaseCamp__c4t10__

#include <iostream>
#include <array>

using namespace std;

const int LENGTH = 3;

void c4t10();

#endif /* defined(__GLBaseCamp__c4t10__) */
