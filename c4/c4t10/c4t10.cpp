//
//  c4t10.cpp
//  GLBaseCamp
//
//  Created by Andrew Pogrebnjak on 06.04.14.
//  Copyright (c) 2014 Andrew Pogrebnjak. All rights reserved.
//

#include "c4t10.h"

void c4t10() {
    array<double, LENGTH> loop;
    double item, summ = 0;
    
    for (int i = 0; i < loop.size(); i++) {
        cout << "Enter time " << i + 1 << ": ";
        cin >> item;
        loop[i] = item;
    }
    for (int i = 0; i < loop.size(); i++) {
        cout << "Time " << i + 1 << ": " << loop[i] << "\n";
        summ += loop[i];
    }
    cout << "Average: " << summ / loop.size() << "\n";
    return;
}