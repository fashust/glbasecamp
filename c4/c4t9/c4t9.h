//
//  c4t9.h
//  GLBaseCamp
//
//  Created by Andrew Pogrebnjak on 05.04.14.
//  Copyright (c) 2014 Andrew Pogrebnjak. All rights reserved.
//

#ifndef __GLBaseCamp__c4t9__
#define __GLBaseCamp__c4t9__

#include <iostream>
#include <string>
#include "structs.h"

using namespace std;

void c4t9();

#endif /* defined(__GLBaseCamp__c4t9__) */
