//
//  c4t9.cpp
//  GLBaseCamp
//
//  Created by Andrew Pogrebnjak on 05.04.14.
//  Copyright (c) 2014 Andrew Pogrebnjak. All rights reserved.
//

#include "c4t9.h"

void c4t9() {
    CandyBar *snacks = new CandyBar[3];
    string brand;
    
    for (int i = 0; i <= 2; i++) {
        brand = "some" + to_string(i);
        (snacks + i)->brand = new char(brand.length() + 1);
        strcpy((snacks + i)->brand, brand.c_str());
        (snacks + i)->calories = 123 * i;
        (snacks + i)->weight = 43 * i;
    }
    for (int i = 0; i <= 2; i++) {
        cout << "Brand: " << snacks[i].brand << "\nWeight: " << snacks[i].weight << "\nCalories: " << snacks[i].calories << "\n\n";
    }
    for (int i = 0; i <= 2; i++) {
        delete[] snacks[i].brand;
    }
    delete [] snacks;
    return;
}