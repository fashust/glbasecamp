//
//  structs.h
//  GLBaseCamp
//
//  Created by Andrew Pogrebnjak on 05.04.14.
//  Copyright (c) 2014 Andrew Pogrebnjak. All rights reserved.
//

#ifndef __GLBaseCamp__structs__
#define __GLBaseCamp__structs__

#include <iostream>

using namespace std;

struct CandyBar {
    char *brand;
    double weight;
    int calories;
};

struct Pizza {
    char *company;
    double diameter;
    double weight;
};


#endif /* defined(__GLBaseCamp__structs__) */
