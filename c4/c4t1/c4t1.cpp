//
//  c4t1.cpp
//  GLBaseCamp
//
//  Created by Andrew Pogrebnjak on 05.04.14.
//  Copyright (c) 2014 Andrew Pogrebnjak. All rights reserved.
//

#include "c4t1.h"

void c4t1() {
    string first_name, last_name;
    char grade = '\0';
    int age = 0;
    
    cout << "What is your first name? ";
    getline(cin, first_name);
    cout << "What is your last name? ";
    getline(cin, last_name);
    cout << "What letter grade do you deserve? ";
    cin >> grade;
    cout << "What is your age? ";
    cin >> age;
    cout << "Name: " << last_name + ", " + first_name << endl;
    cout << "Grade: " << (char)(grade + 1) << endl;
    cout << "Age: " << age << endl;
    return;
}