//
//  c4t6.cpp
//  GLBaseCamp
//
//  Created by Andrew Pogrebnjak on 05.04.14.
//  Copyright (c) 2014 Andrew Pogrebnjak. All rights reserved.
//

#include "c4t6.h"

void c4t6() {
    CandyBar snacks[3];
    string brand;
    
    for (int i = 0; i <= (sizeof(snacks) / sizeof(*snacks)) - 1; i++) {
        brand = "some " + to_string(i);
        snacks[i].brand = new char(brand.length()+1);
        strcpy(snacks[i].brand, brand.c_str());
        snacks[i].weight = 123 * i;
        snacks[i].calories = 43 * i;
    }
    for (int i = 0; i <= (sizeof(snacks) / sizeof(*snacks)) - 1; i++) {
        cout << "Brand: " << snacks[i].brand << "\nWeight: " << snacks[i].weight << "\nCalories: " << snacks[i].calories << "\n\n";
    }
    for (int i = 0; i <= (sizeof(snacks) / sizeof(*snacks)) - 1; i++) {
        delete[] snacks[i].brand;
    }
    return;
}