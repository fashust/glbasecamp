//
//  c4t6.h
//  GLBaseCamp
//
//  Created by Andrew Pogrebnjak on 05.04.14.
//  Copyright (c) 2014 Andrew Pogrebnjak. All rights reserved.
//

#ifndef __GLBaseCamp__c4t6__
#define __GLBaseCamp__c4t6__

#include <iostream>
#include <string>
#include "structs.h"

using namespace std;

void c4t6();

#endif /* defined(__GLBaseCamp__c4t6__) */
