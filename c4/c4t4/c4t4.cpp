//
//  c4t4.cpp
//  GLBaseCamp
//
//  Created by Andrew Pogrebnjak on 05.04.14.
//  Copyright (c) 2014 Andrew Pogrebnjak. All rights reserved.
//

#include "c4t4.h"

void c4t4() {
    string first_name, last_name, result;
    
    cout << "Enter your first name: ";
    getline(cin, first_name);
    cout << "Enter your last name: ";
    getline(cin, last_name);
    result = last_name + ", " + first_name + "\n";
    cout << "Here’s the information in a single string: " << result;
    return;
}