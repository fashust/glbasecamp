//
//  c4t7.h
//  GLBaseCamp
//
//  Created by Andrew Pogrebnjak on 05.04.14.
//  Copyright (c) 2014 Andrew Pogrebnjak. All rights reserved.
//

#ifndef __GLBaseCamp__c4t7__
#define __GLBaseCamp__c4t7__

#include <iostream>
#include <string>
#include "structs.h"

using namespace std;

void c4t7();

#endif /* defined(__GLBaseCamp__c4t7__) */
