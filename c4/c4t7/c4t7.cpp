//
//  c4t7.cpp
//  GLBaseCamp
//
//  Created by Andrew Pogrebnjak on 05.04.14.
//  Copyright (c) 2014 Andrew Pogrebnjak. All rights reserved.
//

#include "c4t7.h"

void c4t7() {
    Pizza *pizza = new Pizza;
    string company;
    
    cout << "Enter company name: ";
    getline(cin, company);
    pizza->company = new char[company.length() + 1];
    strcpy(pizza->company, company.c_str());
    cout << "Enter pizza diameter: ";
    cin >> pizza->diameter;
    cout << "Enter pizza weight: ";
    cin >> pizza->weight;
    cout << "Brand: " << pizza->company << "\nDiameter: " << pizza->diameter << "\nWeight: " << pizza->weight << "\n";
    delete[] pizza->company;
    delete pizza;
    return;
}