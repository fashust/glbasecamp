//
//  main.cpp
//  c4
//
//  Created by Andrew Pogrebnjak on 05.04.14.
//  Copyright (c) 2014 Andrew Pogrebnjak. All rights reserved.
//

#include "c4t1/c4t1.h"
#include "c4t2/c4t2.h"
#include "c4t3/c4t3.h"
#include "c4t4/c4t4.h"
#include "c4t5/c4t5.h"
#include "c4t6/c4t6.h"
#include "c4t7/c4t7.h"
#include "c4t9/c4t9.h"
#include "c4t10/c4t10.h"

int main(int argc, const char * argv[]) {
    c4t1();
    c4t2();
    c4t3();
    c4t4();
    c4t5();
    c4t6();
    c4t7();
    c4t9();
    c4t10();
    return 0;
}