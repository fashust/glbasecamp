//
// Created by Andrew Pogrebnjak on 04.04.14.
// Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#include "c1t4.h"

using namespace std;

void c1t4() {
    int age = 0;
    string age_str;

    cout << "\t\tc1t4" << endl;
    while(true) {
        cout << "Enter yout age: ";
        cin >> age_str;
        if (age_str == "q") {
            break;
        } else {
            try {
                age = stoi(age_str);
            } catch (invalid_argument) {
                cout << "Invalid value \"" << age_str << "\", allowed \"q\" or number\n";
                continue;
            }
            cout << "Your age in months is " << age * 12 << endl;
        }
    }
}