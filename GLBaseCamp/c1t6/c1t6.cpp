//
// Created by Andrew Pogrebnjak on 04.04.14.
// Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#include "c1t6.h"
#define AU_CONST 63240

void c1t6() {
    float light = 0;
    string light_str;

    cout << "\t\tc1t6" << endl;
    while(true) {
        cout << "Enter the number of light years: ";
        cin >> light_str;
        if (light_str == "q") {
            break;
        } else {
            try {
                light = stof(light_str);
            } catch (invalid_argument) {
                cout << "Invalid value \"" << light_str << "\", allowed \"q\" or number\n";
                continue;
            }
            cout << light << " light years = " << ligth_to_astronomical(light) << " astronomical units.\n";
        }
    }
}

float ligth_to_astronomical(float val) {
    return val * AU_CONST;
}
