//
// Created by Andrew Pogrebnjak on 04.04.14.
// Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//


#ifndef __c1t3_H_
#define __c1t3_H_

#include "iostream"

void c1t3();
void three_blind_mice();
void see_how_they_run();

#endif //__c1t3_H_
