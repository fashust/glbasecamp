//
// Created by Andrew Pogrebnjak on 04.04.14.
// Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#include "c1t3.h"

using namespace std;

const char *mice = "Three blind mice\n";
const char *run = "See how they run\n";

void c1t3() {
    cout << "\t\tc1t2" << endl;

    for (int i = 0; i <= 3; i++) {
        if (i < 2) {
            three_blind_mice();
        } else {
            see_how_they_run();
        }
    }
}

void three_blind_mice() {
    cout << mice;
}

void see_how_they_run() {
    cout << run;
}
