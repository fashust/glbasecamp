//
// Created by Andrew Pogrebnjak on 04.04.14.
// Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#include "c1t2.h"

using namespace std;

void c1t2() {
    int furlongs = 0;
    string furlongs_str = "";

    cout << "\t\tc1t2" << endl;
    while (true) {
        cout << "furlongs? >> ";
        cin >> furlongs_str;
        if (furlongs_str == "q") {
            break;
        } else {
            try {
                furlongs = stoi(furlongs_str);
            } catch (invalid_argument) {
                cout << "invalid value: " << furlongs_str << ", should be a number or \"q\"" << endl;
                continue;
            }
            cout << "yards = " << furlongs * diff << endl;
        }
    }
}
