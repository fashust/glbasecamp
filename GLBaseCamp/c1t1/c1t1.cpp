#include "c1t1.h"

const char *name = "Andrew Pogrebnjak";
const char *address = "Kiev, Zoologicheskay st. 1";

void c1t1() {
    std::cout << "\t\tc1t1" << std::endl;
    std::cout << name << std::endl;
    std::cout << address << std::endl;
    std::cout << std::endl;
}