//
//  main.cpp
//  GLBaseCamp
//
//  Created by Andrew Pogrebnjak on 04.04.14.
//  Copyright (c) 2014 Andrew Pogrebnjak. All rights reserved.
//

#include "c1t1/c1t1.h"
#include "c1t2/c1t2.h"
#include "c1t3/c1t3.h"
#include "c1t4/c1t4.h"
#include "c1t5/c1t5.h"
#include "c1t6/c1t6.h"
#include "c1t7/c1t7.h"

int main(int argc, const char * argv[]) {
    c1t1();
    c1t2();
    c1t3();
    c1t4();
    c1t5();
    c1t6();
    c1t7();
    return 0;
}
