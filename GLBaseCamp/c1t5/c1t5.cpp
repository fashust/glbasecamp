//
// Created by Andrew Pogrebnjak on 04.04.14.
// Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#include "c1t5.h"

void c1t5() {
    float fahrenheit = 0;
    string fahrenheit_str;

    cout << "\t\tc1t5" << endl;
    while(true) {
        cout << "Please enter a Celsius value: ";
        cin >> fahrenheit_str;
        if (fahrenheit_str == "q") {
            break;
        } else {
            try {
                fahrenheit = stof(fahrenheit_str);
            } catch (invalid_argument) {
                cout << "Invalid value \"" << fahrenheit_str << "\", allowed \"q\" or number\n";
                continue;
            }
            cout << fahrenheit << " degrees Celsius is " << celsius_to_fahrenheit(fahrenheit) << " degrees Fahrenheit\n";
        }
    }
}

float celsius_to_fahrenheit(float val) {
    return 1.8 * val + 32;
}
