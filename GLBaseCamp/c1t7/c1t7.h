//
// Created by Andrew Pogrebnjak on 04.04.14.
// Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//


#ifndef __c1t7_H_
#define __c1t7_H_

#include "iostream"

using namespace std;

void c1t7();
int convert(string, int);
void output_str(int, int);

#endif //__c1t7_H_
