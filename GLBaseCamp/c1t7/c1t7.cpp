//
// Created by Andrew Pogrebnjak on 04.04.14.
// Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#include "c1t7.h"
#define H_MAX 23
#define M_MAX 59

void c1t7() {
    int hours, minutes = 0;
    string hours_str, minutes_str, str_val;

    cout << "\t\tc1t7" << endl;
    while(true) {
        cout << "Enter the number of hours: ";
        cin >> str_val;
        if (str_val == "q") {
            break;
        } else {
            hours = convert(str_val, H_MAX);
            if (hours < 0) {
                continue;
            }
            while(true) {
                cout << "Enter the number of minutes: ";
                cin >> str_val;
                if (str_val == "q") {
                    break;
                } else {
                    minutes = convert(str_val, M_MAX);
                    if (minutes < 0) {
                        continue;
                    }
                }
                output_str(hours, minutes);
                break;
            }
            if (minutes_str == "q") {
                break;
            }
        }
    }
}

void output_str(int hours, int minutes) {
    cout << "Time: " << hours << ":";
    if (minutes < 10) {
        cout << "0";
    }
    cout << minutes << "\n";
}

int convert(string str_val, int max) {
    int res;

    try {
        res = stoi(str_val);
    } catch (invalid_argument) {
        cout << "Invalid value \"" << str_val << "\", allowed \"q\" or number\n";
        return -1;
    }
    if (res < 0 || res > max) {
        cout << "Invalid value \"" << str_val << "\", min = 0, max = " << max << "\n";
        return -1;
    }
    return res;
}
